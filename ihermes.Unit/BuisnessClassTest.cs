﻿using NUnit.Framework;
namespace ihermes.Unit
{
	[TestFixture(Description="Тестирование бизнес-классов приложения")]
	public class BuisnessClassTest
	{
		[Test(Description="Тестирование серриализаторов и дессериализаторов бизнес-классов приложения")]
		public void SerialiserTestCase()
		{
			/// <summary>
			/// Весь код в данном блоке должен выполнятся без ошибки
			/// </summary>
			Assert.DoesNotThrow(() => { 
				
			});
		}

		[Test(Description="Тестирование контроллера обработки точек доставки")]
		public void DeliveryPointsTestCase() {
			Assert.DoesNotThrow(() => {
				
			});
		}
	}
}

