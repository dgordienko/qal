using System;
namespace ihermes.Core
{	
	/// <summary>
	/// Object header.
	/// </summary>
	public interface IObjectHeader 
	{ 
		/// <summary>
		/// Gets or sets the identifier.
		/// </summary>
		/// <value>The identifier.</value>
		Guid Id { get; set; }
		/// <summary>
		/// Gets or sets the code.
		/// </summary>
		/// <value>The code.</value>
		double Code { get; set; }
		/// <summary>
		/// Gets or sets the adress.
		/// </summary>
		/// <value>The adress.</value>
		IObjectDescription Description { get; set;}
	}
}
