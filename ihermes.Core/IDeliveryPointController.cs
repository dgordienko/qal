namespace ihermes.Core
{
	/// <summary>
	/// Point controller event.
	/// </summary>
	public delegate void PointControllerEvent(object sender, PointsControllerEventArgs arg);

	/// <summary>
	/// Контроллер работы с точками доставки
	/// </summary>
	public interface IDeliveryPointController
	{
		/// <summary>
		/// Событие завершения загрузки точек с сервера
		/// </summary>
		event PointControllerEvent Loaded;

		/// <summary>
		/// Событие завершения загрузки обновленных данных о точках доставки на сервер
		/// </summary>
		event PointControllerEvent Uploaded;
	}
	
}
