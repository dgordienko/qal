using System;
namespace ihermes.Core
{

	/// <summary>
	/// Геррафическая позиция точки.
	/// </summary>
	public interface IPosition
	{
		/// <summary>
		/// Gets or sets the lat.
		/// </summary>
		double Lat { get; set; }

		/// <summary>
		/// Gets or sets the lon.
		/// </summary>
		double Lon { get; set; }

		/// <summary>
		/// Gets or sets the ele.
		/// </summary>
		double Ele { get; set; }
	}
	
}
