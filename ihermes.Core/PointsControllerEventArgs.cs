using System;
using System.Collections.Generic;

namespace ihermes.Core
{
	/// <summary>
	/// Points controller event arguments.
	/// </summary>
	public sealed class PointsControllerEventArgs : EventArgs { 
		/// <summary>
		/// Gets or sets the points.
		/// </summary>
		/// <value>The points.</value>
		public IEnumerable<IDeliveryPoint> Points { get; set; } 
		/// <summary>
		/// Gets or sets the exception.
		/// </summary>
		/// <value>The exception.</value>
		public Exception Exception { get; set; }
	}
	
}
