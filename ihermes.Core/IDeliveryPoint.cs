﻿namespace ihermes.Core
{
	/// <summary>
	/// Точка доставки
	/// </summary>
	public interface IDeliveryPoint
	{
		/// <summary>
		/// Gets or sets the position.
		/// </summary>
		/// <value>The position.</value>
		IPosition Position { get; set; }
		/// <summary>
		/// Gets or sets the header.
		/// </summary>
		/// <value>The header.</value>
		IObjectHeader Header { get; set; }
	}
}

