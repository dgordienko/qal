using System;
namespace ihermes.Core
{
	/// <summary>
	/// Object description.
	/// </summary>
	public interface IObjectDescription 
	{
		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		string Name { get; set; }
	}
}
